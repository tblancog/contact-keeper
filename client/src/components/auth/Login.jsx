import React, { useState, useContext, useEffect } from 'react';
import AuthContext from '../../context/auth/authContext';
import AlertContext from '../../context/alert/alertContext';
const Login = props => {
  const { setAlert } = useContext(AlertContext);
  const { loginUser, error, clearErrors, isAuthenticated } = useContext(AuthContext);

  useEffect(() => {
    if (isAuthenticated) {
      props.history.push('/');
    }
    else if (error === 'Invalid Credentials') {
      setAlert('Invalid Credentials', 'danger');
      clearErrors();
    }
    // eslint-disable-next-line
  }, [error, isAuthenticated, props.history]);

  const [user, setUser] = useState({
    email: '',
    password: '',
  });
  const { email, password } = user;
  const onChange = e => setUser({ ...user, [e.target.name]: e.target.value });
  const onSubmit = e => {
    e.preventDefault();
    if (email === '' || password === '') {
      setAlert('Please enter all fields', 'danger');
    } else {
      loginUser({
        email,
        password
      });
    }
  };
  return (
    <div className="form-container">
      <form onSubmit={onSubmit}>
        <h2>
          Account <span className="text-primary">Login</span>
        </h2>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input name="email" type="text" value={email} onChange={onChange} />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            name="password"
            type="password"
            value={password}
            onChange={onChange}
          />
        </div>
        <input
          type="submit"
          value="Login"
          className="btn btn-primary btn-block"
        />
      </form>
    </div>
  );
};

export default Login;
