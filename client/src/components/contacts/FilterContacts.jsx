import React, { useContext, useRef, useEffect } from 'react';
import ContactContext from '../../context/contact/contactContext';
const FilterContacts = () => {
  const { filterContacts, clearFilter, filtered } = useContext(ContactContext);
  useEffect(() => {
    if (!filtered) {
      text.current.value = '';
    }
  }, [filtered]);

  const text = useRef('');
  const onChange = e => {
    e.preventDefault();
    if (text.current.value !== '') {
      filterContacts(e.target.value);
    } else {
      clearFilter();
    }
  };
  return (
    <div>
      <form>
        <input
          ref={text}
          type="text"
          placeholder="Filter Contacts..."
          onChange={onChange}
        />
      </form>
    </div>
  );
};

export default FilterContacts;
