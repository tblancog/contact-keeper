import React, { Fragment, useState, useContext, useEffect } from 'react';
import ContactContext from '../../context/contact/contactContext';
const ContactForm = () => {
  const contactContext = useContext(ContactContext);
  const { addContact, updateContact, current, clearCurrent } = contactContext;
  useEffect(() => {
    if (current !== null) {
      setContact(current);
    } else {
      setContact({
        name: '',
        phone: '',
        email: '',
        type: 'personal',
      });
    }
  }, [contactContext, current]);
  const [contact, setContact] = useState({
    name: '',
    phone: '',
    email: '',
    type: 'personal',
  });
  const onChange = e => {
    setContact({ ...contact, [e.target.name]: e.target.value });
  };
  const onSubmit = e => {
    e.preventDefault();
    current ? updateContact(contact) : addContact(contact);
    clearAll();
  };
  const clearAll = () => {
    clearCurrent();
  };
  const { name, phone, email, type } = contact;
  return (
    <Fragment>
      <form onSubmit={onSubmit}>
        <div>
          <h2 className="text-primary">
            {!current ? 'Add Contact' : 'Edit Contact'}
          </h2>
          <input
            type="text"
            placeholder="Name"
            name="name"
            value={name}
            onChange={onChange}
          />
          <input
            type="text"
            placeholder="Email"
            name="email"
            value={email}
            onChange={onChange}
          />
          <input
            type="text"
            placeholder="Phone"
            name="phone"
            value={phone}
            onChange={onChange}
          />
          <h5>Contact Type</h5>
          <input
            type="radio"
            id="typePersonal"
            name="type"
            value="personal"
            checked={type === 'personal' && 'checked'}
            onChange={onChange}
          />
          <label htmlFor="typePersonal">Personal</label>{' '}
          <input
            type="radio"
            id="typeProfessional"
            name="type"
            value="professional"
            checked={type === 'professional' && 'checked'}
            onChange={onChange}
          />
          <label htmlFor="typeProfessional">Professional</label>
        </div>
        <div>
          <input
            type="submit"
            value={!current ? 'Add Contact' : 'Save Contact'}
            className="btn btn-primary btn-block"
          />
        </div>
        {current && (
          <div>
            <button className="btn btn-default btn-block" onClick={clearAll}>
              Clear
            </button>
          </div>
        )}
      </form>
    </Fragment>
  );
};

export default ContactForm;
